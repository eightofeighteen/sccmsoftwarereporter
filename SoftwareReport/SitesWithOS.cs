﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftwareReport
{
    class SitesWithOS : Dictionary<String, CounterDictionary>
    {
        public SitesWithOS(SitesWithSystems systems, Dictionary<int, String> osList)
            : base()
        {
            processInformation(systems, osList);
        }

        void processInformation(SitesWithSystems systems, Dictionary<int, String> osList)
        {
            foreach (String site in systems.Keys)
            {
                CounterDictionary siteCounter = new CounterDictionary();
                foreach (int systemID in systems[site])
                {
                    String osName = "Unknown OS";
                    if (osList.ContainsKey(systemID))
                    {
                        osName = osList[systemID];
                    }
                    siteCounter.Increment(osName);
                }
                Add(site, siteCounter);
            }
            collapseList();
        }

        void collapseList()
        {
            /*Dictionary<String, CounterDictionary> newDict = new Dictionary<string, CounterDictionary>();
            foreach (var pair in this)
            {
                CounterDictionary currentDict = pair.Value;
                List<String> osList = currentDict.Keys.ToList();

            }*/
        }

        public override string ToString()
        {
            String s = "";
            foreach (var pair in this)
            {
                s = s + pair.Key + "\n";
                s = s + pair.Value.ToString() + "\n";
            }
            return s;
        }
    }
}
