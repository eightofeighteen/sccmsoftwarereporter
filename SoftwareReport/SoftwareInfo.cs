﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftwareReport
{
    class SoftwareInfo
    {
        DataHandler data;
        SystemOUData ouInfo;
        SitesWithSystems sites;
        SitesWithOS osCounts;
        SitesWithSoftware softwareCounts;

        public SoftwareInfo(DataHandler data)
        {
            this.data = data;
            gatherInfo();
        }

        void gatherInfo()
        {
            ouInfo = new SystemOUData(data);
            sites = ouInfo.SystemsBySite();
            //Console.WriteLine(sites.Information());
            osCounts = new SitesWithOS(sites, data.GetOperatingSystems());
            var swEquivalencies = data.GetSoftwareEquivalencies();
            softwareCounts = new SitesWithSoftware(sites, swEquivalencies, data.SystemsWithSoftware);
            //Console.WriteLine(softwareCounts.ToString());
            //Console.WriteLine(osCounts.ToString());
            SoftwareCountsTable table = new SoftwareCountsTable(sites.Keys.ToList(), osCounts, softwareCounts, data.GetSiteGroupingDefinitions());
            Console.WriteLine(table.ToString());
        }
    }
}
