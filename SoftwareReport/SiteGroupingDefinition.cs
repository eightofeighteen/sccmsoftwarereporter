﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftwareReport
{
    class SiteGroupingDefinition
    {
        public String SiteName { get; set; }
        public String RegEx { get; set; }

        public SiteGroupingDefinition(String siteName, String regEx)
        {
            SiteName = siteName;
            RegEx = regEx;
        }
    }
}
