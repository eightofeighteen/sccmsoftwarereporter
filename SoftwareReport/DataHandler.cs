﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace SoftwareReport
{
    public static class PredicateBuilder
    {
        public static Expression<Func<T, bool>> True<T>() { return f => true; }
        public static Expression<Func<T, bool>> False<T>() { return f => false; }

        public static Expression<Func<T, bool>> Or<T>(this Expression<Func<T, bool>> expr1,
                                                            Expression<Func<T, bool>> expr2)
        {
            var invokedExpr = Expression.Invoke(expr2, expr1.Parameters.Cast<Expression>());
            return Expression.Lambda<Func<T, bool>>
                  (Expression.OrElse(expr1.Body, invokedExpr), expr1.Parameters);
        }

        public static Expression<Func<T, bool>> And<T>(this Expression<Func<T, bool>> expr1,
                                                             Expression<Func<T, bool>> expr2)
        {
            var invokedExpr = Expression.Invoke(expr2, expr1.Parameters.Cast<Expression>());
            return Expression.Lambda<Func<T, bool>>
                  (Expression.AndAlso(expr1.Body, invokedExpr), expr1.Parameters);
        }
    }

    struct ResourceIDSystemOUPair
    {
        int ResourceID { get; set; }
        String SystemOU { get; set; }
    }

    class ResourceIDOSNamePair
    {
        public int ResourceID { get; set; }
        public String OSName { get; set; }

        public ResourceIDOSNamePair(int id, String name)
        {
            ResourceID = id;
            OSName = name;
        }
    }

    class SoftwareEquivalentKeywords
    {
        public String Name { get; set; }
        public List<String> includeKeywords { get; set; }
        public List<String> excludeKeywords { get; set; }

        public SoftwareEquivalentKeywords(String name)
        {
            Name = name;
            includeKeywords = new List<String>();
            excludeKeywords = new List<String>();
        }
    }

    class DataHandler : IDisposable
    {
        SCCMDataDataContext context;
        ConfigDataDataContext configContext;

        public void Dispose()
        {
            context.Dispose();
            configContext.Dispose();
        }

        public DataHandler()
        {
            context = new SCCMDataDataContext();
            configContext = new ConfigDataDataContext();
        }

        // Retrieves site regex definitions from DB.
        public List<SiteGroupingDefinition> GetSiteGroupingDefinitions()
        {
            List<SiteGroupingDefinition> list = new List<SiteGroupingDefinition>();

            var query =
                from a in configContext.ReportSites
                select a;

            foreach (var elem in query)
                list.Add(new SiteGroupingDefinition(elem.Name, elem.RegEx));

            return list;
        }

        public List<v_RA_System_SystemOUName> GetOUNames(int systemID = 0)
        {
            var query =
                from a in context.v_RA_System_SystemOUNames
                select a;
            if (systemID != 0)
                query = query.Where(i => i.ResourceID == systemID);
            return query.ToList();
        }

        public List<int> ResourceIDList()
        {
            var query =
                from a in context.v_R_System_Valids
                select a.ResourceID;


            List<int> list = new List<int>();
            foreach (var elem in query)
            {
                if (!list.Contains(elem))
                    list.Add(elem);
            }
            return list;
        }

        DateTime CutOff
        {
            get
            {
                return DateTime.Now.AddDays(-60);
            }
        }

        public Dictionary<int, String> GetOperatingSystems()
        {
            var query =
                from a in context.v_GS_OPERATING_SYSTEMs
                where (a.TimeStamp >= CutOff)
                orderby (a.TimeStamp) descending
                select new { TS = a.TimeStamp, ResourceID = a.ResourceID, Caption0 = a.Caption0 };
            Dictionary<int, String> dict = new Dictionary<int, String>();
            foreach (var elem in query)
            {
                if (!dict.ContainsKey(elem.ResourceID))
                    dict.Add(elem.ResourceID, elem.Caption0);
            }

            return dict;
        }

        List<SoftwareEquivalentKeywords> GetSoftwareEquivalentKeywords()
        {
            List<SoftwareEquivalentKeywords> list = new List<SoftwareEquivalentKeywords>();

            var keywordsQuery =
                from a in configContext.Keywords
                select a;

            var softwareQuery =
                from a in configContext.SoftwareLists
                select a;

            foreach (var softwareItem in softwareQuery)
            {
                SoftwareEquivalentKeywords keywords = new SoftwareEquivalentKeywords(softwareItem.Name);
                var includeKeywords = keywordsQuery.Where(i => i.SoftwareID == softwareItem.KeywordAllowed);
                var excludeKeywords = keywordsQuery.Where(i => i.SoftwareID == softwareItem.KeywordDisallowed);
                foreach (var includeKeyword in includeKeywords)
                {
                    keywords.includeKeywords.Add(includeKeyword.Keyword1);
                }
                foreach (var excludeKeyword in excludeKeywords)
                {
                    keywords.excludeKeywords.Add(excludeKeyword.Keyword1);
                }
                list.Add(keywords);
            }

            /*SoftwareEquivalentKeywords lync = new SoftwareEquivalentKeywords("Lync");
            lync.includeKeywords.Add("lync");
            lync.includeKeywords.Add("office communicator");
            lync.includeKeywords.Add("Skype for Business");
            lync.excludeKeywords.Add("Update");
            lync.excludeKeywords.Add("Online");
            list.Add(lync);

            SoftwareEquivalentKeywords office = new SoftwareEquivalentKeywords("Office");
            office.includeKeywords.Add("excel");
            office.includeKeywords.Add("word");
            office.includeKeywords.Add("powerpoint");
            office.includeKeywords.Add("outlook");
            office.excludeKeywords.Add("Update");
            office.excludeKeywords.Add("Viewer");
            list.Add(office);*/

            return list;
        }

        public Dictionary<String, List<String>> GetSoftwareEquivalencies()
        {
            Dictionary<String, List<String>> equivalencies = new Dictionary<String, List<String>>();
            var equivalencyInfo = GetSoftwareEquivalentKeywords();

            /*var query =
                from a in context.v_GS_ADD_REMOVE_PROGRAMs
                where (a.DisplayName0.Contains("lync") || a.DisplayName0.Contains("office communicator") || a.DisplayName0.Contains("Skype for Business"))
                select a.DisplayName0;
            query = query.Distinct();
            query = query.Where(i => !i.Contains("Update"));
            query = query.Where(i => !i.Contains("Online"));

            equivalencies.Add("Lync", query.ToList());

            query =
                from a in context.v_GS_ADD_REMOVE_PROGRAMs
                where (a.DisplayName0.Contains("excel"))
                select a.DisplayName0;
            query = query.Distinct();
            query = query.Where(i => !i.Contains("Update"));
            query = query.Where(i => !i.Contains("Viewer"));

            equivalencies.Add("Office", query.ToList());*/
            
            foreach (var softwareItem in equivalencyInfo)
            {
                var query32 =
                    from a in context.v_GS_ADD_REMOVE_PROGRAMs
                    where (a.TimeStamp >= CutOff)
                    orderby (a.TimeStamp) descending
                    select a.DisplayName0;
                query32 = query32.Distinct();
                var predicate32 = PredicateBuilder.False<String>();
                foreach (var includeItem in softwareItem.includeKeywords)
                {
                    string temp = includeItem;
                    predicate32 = predicate32.Or(i => i.Contains(temp));
                }
                query32 = query32.Where(predicate32);
                foreach (var excludeItem in softwareItem.excludeKeywords)
                {
                    query32 = query32.Where(i => !i.Contains(excludeItem));
                }

                equivalencies.Add(softwareItem.Name, query32.ToList());

                var query64 =
                    from a in context.v_GS_ADD_REMOVE_PROGRAMS_64s
                    where (a.TimeStamp >= CutOff)
                    orderby (a.TimeStamp) descending
                    select a.DisplayName0;
                query64 = query64.Distinct();
                var predicate64 = PredicateBuilder.False<String>();
                foreach (var includeItem in softwareItem.includeKeywords)
                {
                    string temp = includeItem;
                    predicate64 = predicate32.Or(i => i.Contains(temp));
                }
                query64 = query64.Where(predicate32);
                foreach (var excludeItem in softwareItem.excludeKeywords)
                {
                    query64 = query64.Where(i => !i.Contains(excludeItem));
                }

                var equiv = equivalencies[softwareItem.Name];
                foreach (var elem in query64)
                {
                    if (!equiv.Contains(elem))
                        equiv.Add(elem);
                }
                
            }
            

            return equivalencies;
        }

        public List<int> SystemsWithSoftware(List<String> software)
        {
            var query =
                from a in context.v_GS_ADD_REMOVE_PROGRAMs
                where (/*a.TimeStamp >= CutOff && */software.Contains(a.DisplayName0))
                select a.ResourceID;
            query = query.Distinct();

            var query64 =
                from a in context.v_GS_ADD_REMOVE_PROGRAMS_64s
                where (/*a.TimeStamp >= CutOff && */software.Contains(a.DisplayName0))
                select a.ResourceID;
            query64 = query64.Distinct();

            List<int> list = new List<int>();
            foreach (int elem in query)
            {
                if (!list.Contains(elem))
                    list.Add(elem);
            }
            foreach (int elem in query64)
            {
                if (!list.Contains(elem))
                    list.Add(elem);
            }
            

            //return query.ToList();
            return list;
        }
    }
}
