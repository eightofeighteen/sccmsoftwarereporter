﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftwareReport
{
    class SitesWithSystems : Dictionary<String, List<int>>
    {
        public SitesWithSystems(SystemOUData ouData)
            : base()
        {
            getData(ouData);
        }

        void getData(SystemOUData ouData)
        {
            List<String> siteNames = new List<String>();
            foreach (var pair in ouData)
            {
                if (!siteNames.Contains(pair.Value))
                    siteNames.Add(pair.Value);
            }

            foreach (var site in siteNames)
            {
                List<int> systems = new List<int>();
                var systemIDs = ouData.Where(i => i.Value == site);
                foreach (var elem in systemIDs)
                {
                    systems.Add(elem.Key);
                }
                Add(site, systems);
            }
        }

        public String Information()
        {
            String s = "";
            foreach (var pair in this)
            {
                s = s + pair.Key + "\n";
                foreach (var elem in pair.Value)
                {
                    s = s + "\t" + elem + "\n";
                }
            }
            return s;
        }
    }
}
