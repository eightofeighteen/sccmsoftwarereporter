﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftwareReport
{
    class SystemOUData : Dictionary<int , String>
    {
        DataHandler data;

        public SystemOUData() : base()
        {
            
        }

        public SystemOUData(DataHandler data)
            : base()
        {
            this.data = data;
            populateInformation();
        }

        void populateInformation()
        {
            List<v_RA_System_SystemOUName> rawOUInfo = data.GetOUNames();
            List<int> uniqueResourceIDs = new List<int>();
            foreach (var elem in rawOUInfo)
            {
                if (!uniqueResourceIDs.Contains(elem.ResourceID))
                    uniqueResourceIDs.Add(elem.ResourceID);
            }

            foreach (int id in uniqueResourceIDs)
            {
                var OUsForSystem = rawOUInfo.Where(i => i.ResourceID == id);
                if (OUsForSystem.ToList().Count > 0)
                {
                    String ou = OUsForSystem.First().System_OU_Name0;
                    foreach (var elem in OUsForSystem)
                    {
                        if (elem.System_OU_Name0.Length < ou.Length)
                            ou = elem.System_OU_Name0;
                    }
                    Add(id, ou);
                }
            }
        }

        public String Information()
        {
            String s = "";
            foreach (KeyValuePair<int, String> pair in this)
            {
                s = s + pair.Key + "\t" + pair.Value + "\n";
            }
            return s;
        }

        public SitesWithSystems SystemsBySite()
        {
            SitesWithSystems info = new SitesWithSystems(this);
            return info;
        }
    }
}
