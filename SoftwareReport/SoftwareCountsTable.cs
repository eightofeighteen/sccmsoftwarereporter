﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace SoftwareReport
{
    class SoftwareCountsTable
    {
        List<String> sites;
        SitesWithOS osCounts;
        SitesWithSoftware softwareCounts;

        List<String> headers = new List<String>();
        Dictionary<String, Dictionary<String, int>> counts = new Dictionary<String, Dictionary<String, int>>();
        List<SiteGroupingDefinition> siteDefinitions;

        public SoftwareCountsTable(List<String> sites, SitesWithOS osCounts, SitesWithSoftware softwareCounts, List<SiteGroupingDefinition> siteDefinitions)
        {
            this.sites = sites;
            this.osCounts = osCounts;
            this.softwareCounts = softwareCounts;
            this.siteDefinitions = siteDefinitions;
            populateInformation();
        }

        void populateInformation()
        {
            foreach (var pair in osCounts)
            {
                List<String> headings = pair.Value.Keys.ToList();
                foreach (String heading in headings)
                {
                    if (!headers.Contains(heading))
                        headers.Add(heading);
                }
            }
            foreach (var pair in softwareCounts)
            {
                foreach (String heading in pair.Value.Keys)
                {
                    if (!headers.Contains(heading))
                        headers.Add(heading);
                }
            }

            foreach (String site in sites)
            {
                Dictionary<String, int> countLine = new Dictionary<String, int>();
                foreach (String header in headers)
                {
                    countLine.Add(header, 0);
                }
                counts.Add(site, countLine);
            }

            foreach (String site in sites)
            {
                var countLine = counts[site];
                var osCountLine = osCounts[site];
                foreach (var count in osCountLine)
                {
                    countLine[count.Key] = count.Value;
                }
                foreach (var count in softwareCounts[site])
                {
                    countLine[count.Key] = count.Value;
                }
            }
            CollapseInformation();
        }
        /// <summary>
        /// Collapsed lines of the counts table into sites, as defined by the regex of siteDefinitions.
        /// </summary>
        private void CollapseInformation()
        {
            foreach (SiteGroupingDefinition siteDefinition in siteDefinitions)
            {
                Dictionary<String, int> tempCounts = new Dictionary<String, int>();
                List<String> keys = counts.Keys.ToList();
                List<String> matchedKeys = new List<String>();

                foreach (String key in keys)
                {
                    if (RegExDoesMatch(key, siteDefinition.RegEx))
                        matchedKeys.Add(key);
                }

                foreach (String key in matchedKeys)
                {
                    tempCounts = AddLines(tempCounts, counts[key]);
                }

                if (tempCounts.Count > 0)
                {
                    foreach (String key in matchedKeys)
                        counts.Remove(key);
                    counts.Add(siteDefinition.SiteName, tempCounts);
                }
            }
        }

        Dictionary<String, int> AddLines(Dictionary<String, int> line1, Dictionary<String, int> line2)
        {
            Dictionary<String, int> line = new Dictionary<String, int>();
            //if (line1.Count != line2.Count)
            //    return null;
            foreach (var pair in line2)
            {
                if (line1.Keys.Contains(pair.Key))
                    line.Add(pair.Key, line1[pair.Key] + line2[pair.Key]);
                else
                    line.Add(pair.Key, line2[pair.Key]);
            }
            return line;
        }

        List<int> Totals()
        {
            List<int> totals = new List<int>();

            for (int i = 0; i < headers.Count; i++)
            {
                int sum = 0;
                foreach (var countPair in counts)
                {
                    sum += countPair.Value[headers[i]];
                }
                totals.Add(sum);
            }

            return totals;
        }

        private bool RegExDoesMatch(String str, String re)
        {
            Regex r = new Regex(re, RegexOptions.IgnoreCase);
            Match m = r.Match(str);

            return m.Success;
        }

        public override string ToString()
        {
            String s = "";
            for (int i = 0; i < headers.Count; i++)
                s = s + ",\"" + headers[i] + "\"";
            s += "\n";
            //foreach (String site in sites)
            foreach (String site in counts.Keys)
            {
                s = s + site;
                for (int i = 0; i < headers.Count; i++)
                {
                    var line = counts[site];
                    var count = line[headers[i]];
                    s = s + "," + count;
                }
                s += "\n";
            }
            var totals = Totals();
            s += "Totals";
            for (int i = 0; i < totals.Count; i++)
                s = s + ",\"" + totals[i] + "\"";
            s += "\n";

            return s;
        }
    }
}
