﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftwareReport
{
    public delegate List<int> delegateSystemsWithSoftware(List<String> software);

    class SitesWithSoftware : Dictionary<String, CounterDictionary>
    {
        public SitesWithSoftware(SitesWithSystems systems, Dictionary<String, List<String>> softwareEquivalencies, delegateSystemsWithSoftware dataFunction) : base()
        {
            processInformation(systems, softwareEquivalencies, dataFunction);
        }

        void processInformation(SitesWithSystems systems, Dictionary<String, List<String>> softwareEquivalencies, delegateSystemsWithSoftware dataFunction)
        {
            Dictionary<String, List<int>> softwareList = new Dictionary<String, List<int>>();
            foreach (String name in softwareEquivalencies.Keys)
            {
                List<int> compsWithSoftware = dataFunction(softwareEquivalencies[name]);
                softwareList.Add(name, compsWithSoftware);
            }
            foreach (String site in systems.Keys)
            {
                CounterDictionary cd = new CounterDictionary();
                foreach (String name in softwareList.Keys)
                {
                    List<int> siteSoftware = softwareList[name].Intersect(systems[site]).ToList();
                    cd.Add(name, siteSoftware.Count);
                }
                Add(site, cd);
            }
        }

        public override string ToString()
        {
            String s = "";
            foreach (var pair in this)
            {
                s = s + pair.Key + "\n";
                s = s + pair.Value.ToString() + "\n";
            }
            return s;
        }
    }
}
