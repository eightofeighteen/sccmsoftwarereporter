﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftwareReport
{
    class Program
    {
        static void EndProg()
        {
            Console.WriteLine("Program complete.");
            Console.ReadLine();
        }

        static void Run()
        {
            DataHandler data = new DataHandler();
            //SystemOUData ouInfo = new SystemOUData(data);
            //Console.WriteLine(ouInfo.Information());
            //SitesWithSystems sites = ouInfo.SystemsBySite();
            //Console.WriteLine(sites.Information());

            SoftwareInfo info = new SoftwareInfo(data);
            data.Dispose();
        }

        static void Main(string[] args)
        {
            Run();
            EndProg();
        }
    }
}
