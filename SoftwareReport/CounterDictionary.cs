﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftwareReport
{
    class CounterDictionary : Dictionary<String, int>
    {
        public CounterDictionary() : base()
        {

        }

        public void Increment(String key, int value = 1)
        {
            if (!ContainsKey(key))
                Add(key, value);
            else
                this[key]++;
        }

        public override string ToString()
        {
            String s = "";
            foreach (var pair in this)
            {
                s = s + pair.Key + "\t" + pair.Value + "\n";
            }
            return s;
        }
    }
}
